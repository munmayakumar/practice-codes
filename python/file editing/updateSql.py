# -*- coding: utf-8 -*-
"""
Created on Sun Jul 19 14:21:52 2020

@author: munmaya
"""

import os
from shutil import copyfile



def createMidContent(inputFile):
    inFile = open(inputFile, 'r')
    content = inFile.read().split()
    midContent = []
    for i in content:
        midContent.append('\t\t'+"'"+i+"',")
    inFile.close()
    return midContent


       
def createNewFileContent(filename):
    f = open(filename, 'r')
    content = f.read().split('\n')
    fileContentBegin=[]
    fileContentEnd=[]

    for line in content:
        if line.find("owner in") != -1:
            fileContentBegin.append(line)
            break;
        else:
            fileContentBegin.append(line)

    content.reverse();

    for line in content:
        if line.find("!!!") != -1:
            fileContentEnd.append(line)
            break;
        else:
            fileContentEnd.append(line)
    
    fileContentEnd.reverse()
    content.reverse();
        
    fileContentMid = createMidContent('schema.dat')
    
    if fileContentBegin == fileContentEnd:
        contentNew = content
    else:
        contentNew = fileContentBegin + fileContentMid + fileContentEnd
    return content, contentNew



def writeToFile(contentList, filename):
    wr = open(filename, 'w')
    for i in contentList:
        wr.write(i)
        wr.write('\n')
    wr.close()



def main():
    directory = os.getcwd()
    #print("Current working directory : " + cwd)
      
    for filename in os.listdir(directory):
        if filename.endswith(".sql"):
            print("Modification started for file: %s" %filename)
            content, contentNew = createNewFileContent(filename)
            backupFileName = filename+'.old'
            copyfile(filename, backupFileName)
            print("Backup file created: %s" %backupFileName)
            writeToFile(contentNew, filename)
            print("Modification completed for file: %s" %filename)
            

if __name__ == '__main__':
    main()
