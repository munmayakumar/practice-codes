# -*- coding: utf-8 -*-
"""
Created on Sat Aug 15 14:39:19 2020

@author: admin
"""


stuff = {'rope': 1, 'torch': 6, 'gold coin': 42, 'dagger': 1, 'arrow': 12}

newStuff = ['gun', 'knife', 'torch', 'gun', 'gun', 'gold coin', 'arrow',
            'arrow','arrow','arrow','arrow','arrow', 'rope', 'knife', 'tissue',
            'rope','dagger','pen drive','gold coin','rope','arrow','gold coin',
            'arrow','gun','dagger','bullet','bullet','rope','knife','rope',
            'tissue','gun','torch', 'branch', 'gold coin']

def addInventory(inventory, newStuff):
    for item in newStuff:
        inventory.setdefault(item, 0)
        inventory[item] += 1

def displayInventory(inventory):
    total = 0
    for k,v in inventory.items():
        print(k + ' : ' + str(v))
        total += v
    print('Total : ' + str(total))
    
addInventory(stuff, newStuff)
displayInventory(stuff)