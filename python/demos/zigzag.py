# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 22:43:44 2020

@author: admin
"""

import time, sys

i=0
try:
    while True:
        startStr = '.' * i
        endStr = '.' * (5-i)
        print(startStr,'##################',endStr,sep='')
        time.sleep(0.1)
        
        if i == 0:
            increaseTrend = True
        
        if i == 5:
            increaseTrend = False
        
        if increaseTrend:
            i += 1
        else:
            i -= 1
except KeyboardInterrupt:
    print("Interrupted by User!!!")
    sys.exit()

        
            
            
   