# -*- coding: utf-8 -*-
"""
Created on Fri Aug 14 21:32:41 2020

@author: admin
"""

import random, time

theBoard = {'topL' : ' ',
            'topM' : ' ',
            'topR' : ' ',
            'midL' : ' ',
            'midM' : ' ',
            'midR' : ' ',
            'lowL' : ' ',
            'lowM' : ' ',
            'lowR' : ' '
            }

availableMoves = ['topL','topM','topR','midL','midM','midR','lowL','lowM','lowR']


def printBoard(board):
    print('\n')
    print(' %s | %s | %s ' %(board['topL'], board['topM'], board['topR']))
    print('---+---+---')
    print(' %s | %s | %s ' %(board['midL'], board['midM'], board['midR']))
    print('---+---+---')
    print(' %s | %s | %s ' %(board['lowL'], board['lowM'], board['lowR']))
    
  
def decideWinner(board):
    if board['topL'] == board['topM'] and board['topM'] == board['topR'] and board['topL'] != ' ':
        return  board['topL']
    elif board['midL'] == board['midM'] and board['midM'] == board['midR'] and board['midL'] != ' ':
        return board['midL']
    elif board['lowL'] == board['lowM'] and board['lowM'] == board['lowR'] and board['lowL'] != ' ':
        return board['lowL']
    elif board['topL'] == board['midL'] and board['midL'] == board['lowL'] and board['topL'] != ' ':
        return board['topL']
    elif board['topM'] == board['midM'] and board['midM'] == board['lowM'] and board['topM'] != ' ':
        return board['topM']
    elif board['topR'] == board['midR'] and board['midR'] == board['lowR'] and board['topR'] != ' ':
        return board['topR']
    elif board['topL'] == board['midM'] and board['midM'] == board['lowR'] and board['topL'] != ' ':
        return board['topL']
    elif board['topR'] == board['midM'] and board['midM'] == board['lowL'] and board['topR'] != ' ':
        return board['topR']
    else:
        return 'KeepPlaying'
    

def registerMove(move, moveVal):
    availableMoves.remove(move)
    theBoard[move] = moveVal
    

def main():
    print('\n... Welcome to Tic Tac Toe ...\n')
    
    time.sleep(1)
    
    while True:
        print('Choose your symbol (X or O): ', end = '')
        userSymbol = input()
        
        time.sleep(1)
        
        if userSymbol == 'X':
            sysSymbol = 'O'
            print('\n.. You play first ..')
            printBoard(theBoard)
            break
        elif userSymbol == 'O':
            sysSymbol = 'X'
            print('\n.. I play first ..')
            time.sleep(1)
            randInt = random.randint(0,len(availableMoves)-1)
            registerMove(availableMoves[randInt], sysSymbol)
            printBoard(theBoard)
            break
        else:
            print("\nInvalid selection, let's try again.")
            
    gameStatus = 'KeepPlaying'
    
    while len(availableMoves) > 0:
        time.sleep(1)
        
        print('\nAvailable Moves : ', end = '')
        print(availableMoves)
        if len(availableMoves) > 1:
            print('\nEnter your move for %s : ' %userSymbol, end = '')
            userMove = input()
        else:
            userMove = availableMoves[0]
        
        time.sleep(1)
        
        if userMove in availableMoves:
            registerMove(userMove, userSymbol)
            gameStatus = decideWinner(theBoard)
            printBoard(theBoard)
            if gameStatus != 'KeepPlaying':
                print('\nCongratulations.. You Win!!')
                break
        else:
            print("Entered move is either incorrect or unavailable. Let's try again.")
            continue
        
        time.sleep(1)
        
        if len(availableMoves) > 0:
            print("\nHere's my move..")
            randInt = random.randint(0,len(availableMoves)-1)
            registerMove(availableMoves[randInt], sysSymbol)
            gameStatus = decideWinner(theBoard)
            printBoard(theBoard)
            if gameStatus != 'KeepPlaying':
                print('\nHard Luck.. I Win!!')
                break
            if len(availableMoves) == 0:
                print("\nIt's a TIE!! Seems like I am as good as you are ;)")
        else:
            print("\nIt's a TIE!! Seems like I am as good as you are ;)")

    time.sleep(1)
    print('\nThanks for playing. Have a good day!\n')
        
        
if __name__ == '__main__':
    main()
            
    
    
    

        
        
    
