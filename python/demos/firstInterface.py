# -*- coding: utf-8 -*-
"""
Created on Sun Dec 27 23:27:02 2020

@author: admin
"""


class Card(object):
    """Represents a standard playing card.
    """
    suit_names = ['Clubs', 'Diamonds', 'Hearts', 'Spades']
    rank_names = [None, 'Ace', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King']
    
    def __init__(self, suit=0, rank=2):
        self.suit = suit
        self.rank = rank
        
