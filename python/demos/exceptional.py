# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 22:16:09 2020

@author: admin
"""

import sys

DIGIT_MAP = {
    'zero' : '0',
    'one' : '1',
    'two' : '2',
    'three' : '3',
    'four' : '4',
    'five' : '5',
    'six' : '6',
    'seven' : '7',
    'eight' : '8',
    'nine' : '9',
    }

def convert_v1(s):
    try:
        number = ''
        for token in s:
            number += DIGIT_MAP[token]
        x = int(number)
        print("Conversion Successful! x = ", x)
    except KeyError:
        x = -1
        print("Conversion Failed! x = ", x)
    except TypeError:
        x = -1
        print("Please input an iterable type object!")
    return x


def convert_v2(s):
    try:
        x = -1
        number = ''
        for token in s:
            number += DIGIT_MAP[token]
        x = int(number)
        print("Conversion Successful! x = ", x)
    except (KeyError, TypeError):
        print("Conversion Failed! x = ", x)
    return x


def convert_v3(s):
    try:
        number = ''
        for token in s:
            number += DIGIT_MAP[token]
        return int(number)
    except (KeyError, TypeError, ValueError):
        return -1


def convert_v4(s):
    try:
        number = ''
        for token in s:
            number += DIGIT_MAP[token]
        return int(number)
    except (KeyError, TypeError, ValueError) as e:
        print(f"Conversion Error! {e!r}", file=sys.stderr) 
        return -1
