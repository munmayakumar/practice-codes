# -*- coding: utf-8 -*-
"""
Created on Sun Sep  6 19:38:51 2020

@author: admin
"""

#! python3
# myClip.py : A multi-keyboard program

TEXT = {"agree" : """Yes, I agree. That sounds fine to me.""",
        "busy" : """Sorry, can we do this late rthis week or next week?""",
        "upsell" : """Would you consider making this a monthly donation?"""}

import sys, pyperclip
if len(sys.argv) < 2:
    print("Usage: python myClip.py [keyphrase] - copy phrase text")
    sys.exit()
    
keyphrase = sys.argv[1] #first command line argument in the keyphrase

if keyphrase in TEXT:
    pyperclip.copy(TEXT[keyphrase])
    print("Text for %s copied to clipboard" %keyphrase)
else:
    print("There is no text for %s" %keyphrase)