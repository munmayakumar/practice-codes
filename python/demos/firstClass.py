# -*- coding: utf-8 -*-
"""
Created on Fri Dec 25 19:32:44 2020

@author: admin
"""


class A(object):
    def __init__(self, items=None):
        if items is None:
            items = list()
        else:
            self.items = items
            
    def show(self):
        print('name = "%s"' %self.items)
        
a = A('manvi')
a.show()

a1 = A(['khushi', 'manvi'])
a1.show()

'''a2 = A(None)
a2.show()'''

#----------------------------------------

class Point(object):
    x = int()
    y = int()

p = Point()
p.x = 10
p.y = 20

def print_point(pp):
    print('x=%d, y=%d' %(pp.x,pp.y))
    
print_point(p)
    
class Rectangle(object):
    """width = int()
    height = int()
    corner = Point()"""
    pass
    
r1 = Rectangle()
r1.width = 22
r1.height = 11
r1.corner = Point()
r1.corner.x = 5
r1.corner.y = 6

print_point(r1.corner)

import copy

r2 = copy.copy(r1)
print('r1 is r2 : %s' %str(r1 is r2))
print('r1.corner is r2.corner : %s' %str(r1.corner is r2.corner))

r1.corner.x = 55
r1.corner.y = 66

print_point(r2.corner)

r3 = copy.deepcopy(r1)
print('r1 is r3 : %s' %str(r1 is r3))
print('r1.corner is r3.corner : %s' %str(r1.corner is r3.corner))

r1.corner.x = 555
r1.corner.y = 666

print_point(r3.corner)

#----------------------------------------

class Time(object):
    pass
    
time1 = Time()
time1.hour = 11
time1.minutes = 58
time1.seconds = 0


def print_time(t):
    print('Time is - %.2d:%.2d:%.2d' %(t.hour, t.minutes, t.seconds))
    
print_time(time1)


time2 = Time()
time2.hour = 11
time2.minutes = 23
time2.seconds = 3

def is_after(t1, t2):
    print ((t1.hour, t1.minutes, t1.seconds) > (t2.hour, t2.minutes, t2.seconds))

is_after(time1, time2)

def increment(time, seconds):
    r = copy.deepcopy(time)
    
    r.seconds += seconds
    r.minutes += r.seconds//60
    r.hour += r.minutes//60
    r.seconds %= 60
    r.minutes %= 60
    r.hour %= 24

    return r
    
time3 = increment(time1, -62)

print_time(time3)
print(r3.__dict__)


#----------------------------------------


class Kangaroo(object):
    def __init__(self, contents = None):
        if contents == None:
            contents = list()
        self.pouch_contents = contents
        
    def put_in_pouch(self, item):
        self.pouch_contents.append(item)
    
    def __str__(self):
        return ';'.join(self.pouch_contents)


kanga = Kangaroo()
kanga.put_in_pouch('hello')
kanga.put_in_pouch('kanga')
roo = Kangaroo()

print(kanga)
print(roo)

print('end of file')