# -*- coding: utf-8 -*-
"""
Created on Sun Aug 16 22:27:38 2020

@author: admin
"""


picnicItems = {'sandwiches': 4, 'apples': 12, 'cups': 4, 'cookies': 8000}


def printPicnic(items, lWidth, rWidth):
    print('PICNIC ITEMS'.ljust(lWidth,'.') + 'QUANTITY'.rjust(rWidth,'.'))
    for k,v in items.items():
        print(k.title().ljust(lWidth,'.') + str(v).rjust(rWidth,'.'))
    
        
printPicnic(picnicItems, 20, 10)

print(chr(2835))