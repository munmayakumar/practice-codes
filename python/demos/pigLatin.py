# -*- coding: utf-8 -*-
"""
Created on Sun Sep  6 23:03:16 2020

@author: admin
"""


print('Please enter message in English to translate to PigLatin: ')
message = input()

vowels = ('a', 'e', 'i', 'o', 'u')
wordEng = message.split()
wordPL = []

for word in wordEng:
    if word[0].lower() in vowels:
        wordPL.append(word+'yay')
    else:
        isVowelCheck = 'Yes'
        i = 0
        while isVowelCheck == 'Yes':
            if word[i].lower() in vowels:
                isVowelCheck = 'Yes'
                #print(word[i]+' xx')
            else:
                isVowelCheck = 'No'
                #print(word[i]+' yy')
            i += 1
        wordPL.append(word[i:]+word[:i]+'ay')

messagePL = ' '.join(wordPL)
print('Your message in PigLatin is : %s' %messagePL)