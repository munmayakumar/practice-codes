# -*- coding: utf-8 -*-
"""
Created on Sat Aug  8 16:31:26 2020

@author: admin
"""


def collatz(number):
    if number%2 == 0:
        return number // 2 
    else:
        return number * 3 + 1
    

def main():
    while True:
        try:
            print("Enter a number of your choice : ")
            userInput = input()
            inputNum = int(userInput)
            break
        except ValueError:
            print("That's not a number I know. Let's try again.")
    
    print("-----")
    returnNum = 0
    returnNum = collatz(inputNum)
    while True:
        print(returnNum)
        if returnNum == 1:
            break
        returnNum = collatz(returnNum)

        
if __name__ == "__main__":
    main()
        