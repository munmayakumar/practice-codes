# -*- coding: utf-8 -*-
"""
Created on Sun Oct  4 11:07:21 2020

@author: admin
"""


def inPhoneNumber(text):
    if len(text) != 12:
        return False
    
    for i in range(0,3):
        if not text[i].isdecimal():
            return False
    
    if text[3] != '-':
        return False
    
    for i in range(4,7):
        if not text[i].isdecimal():
            return False
        
    if text[7] != '-':
        return False
    
    for i in range(8,12):
        if not text[i].isdecimal():
            return False
        
    return True

print('415-255-8978' + ' : ' + str(inPhoneNumber('415-255-8978')))
print('Munmaya' + ' : ' +  str(inPhoneNumber('Munmaya')))
print('415-255-89784' + ' : ' + str(inPhoneNumber('415-255-89784')))