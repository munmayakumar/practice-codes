# -*- coding: utf-8 -*-
"""
Created on Sun Sep  6 20:29:36 2020

@author: admin
"""


#! python3
#bulletPointAdder.py : Added bullet points to the start 
# of each line of text on the clipboard

import pyperclip

text = pyperclip.paste()
text = '*'+text.replace('\n', '\n*')
pyperclip.copy(text)