# -*- coding: utf-8 -*-
"""
Created on Sun Aug  2 12:38:22 2020

@author: admin
"""


import random

print("*** Welcome to ROCK-PAPER-SCISSORS game ***")

userWins = 0
sysWins = 0
ties = 0
msgRockScissors = "ROCK curshes SCISSORS"
msgScissorsPaper = "SCISSOR cuts PAPER"
msgPaperRock = "PAPER covers ROCK"
msgTie = "It's a tie"

while True:
    print("\nEnter your choice: (r)ock, (p)aper, (s)cissors or (q)uit:")
    userInput = input()
    
    if userInput == "r":
        userSelection = "ROCK"
        userNum = 1
    elif userInput == "p":
        userSelection = "PAPER"
        userNum = 2
    elif userInput == "s":
        userSelection = "SCISSORS"
        userNum = 3
    elif userInput == "q":
        print("*** Thanks for playing! ***")
        break
    else:
        print("Incorrect choice, let's try again.")
        continue
        
    randomNum = random.randint(1,3)
    if randomNum == 1:
        randomSelection = "ROCK"
    elif randomNum == 2:
        randomSelection = "PAPER"
    elif randomNum == 3:
        randomSelection = "SCISSORS"
    
    if userSelection == "ROCK" and randomSelection == "SCISSORS" :
        message = msgRockScissors + ". You Win!"
        userWins += 1
    elif userSelection == "SCISSORS" and randomSelection == "ROCK" :
        message = msgRockScissors + ". I Win!"
        sysWins += 1
    elif userSelection == "SCISSORS" and randomSelection == "PAPER" :
        message = msgScissorsPaper + ". You Win!"
        userWins += 1
    elif userSelection == "PAPER" and randomSelection == "SCISSORS" :
        message = msgScissorsPaper + ". I Win!"
        sysWins += 1
    elif userSelection == "PAPER" and randomSelection == "ROCK" :
        message = msgPaperRock + ". You Win!"
        userWins += 1
    elif userSelection == "ROCK" and randomSelection == "PAPER" :
        message = msgPaperRock + ". I Win!"
        sysWins += 1
    else:
        message = msgTie + ". No one wins."
        ties += 1 
        
    print("Your %s versus my %s... %s" % (userSelection, randomSelection, message))
    print()
    print("%d Wins, %d Losses, %d Ties" % (userWins, sysWins, ties))
    print()