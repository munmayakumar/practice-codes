"""Retrieve and print words from a URL.

Usage:
    python words.py <URL>

"""

import sys
from urllib.request import urlopen


def fetch_words(url):
    """Fetch a list of words from a URL.
    
    Args:
        url: The URL of a UTF-8 text document. Example: http://sixty-north.com/c/t.txt
        
    Returns:
        A list of string containing the words from the document.
    """
    #story = urlopen('http://sixty-north.com/c/t.txt')
    story = urlopen(url)
    story_words = []
    for line in story:
        line_words = line.decode('utf-8').split()
        for word in line_words:
            story_words.append(word)
    story.close()
    return story_words


def print_items(item):
    """Print items one per line.
    
    Args:  
        An iterable series of printable items.
    """
    for i in item:
        print(i)
    
#print(__name__)


def main(url):
    """Print each word from a text document at a URL.
    
    Args:
        url: The URL of a UTF-8 text document. Example: http://sixty-north.com/c/t.txt
    """
    #url = sys.argv[1]
    words = fetch_words(url)
    print_items(words)


if __name__ == '__main__':
    #fetch_words()
    #main()
    main(sys.argv[1])
