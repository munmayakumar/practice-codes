# -*- coding: utf-8 -*-
"""
Created on Sun Aug  2 12:00:11 2020

@author: admin
"""

import random

num=random.randint(1,20)
print("I am thinking of a number between 1 and 20.")
chances=0

while True:
    print("\nTake a guess:")
    guess = input()
    
    try:
        guessNum = int(guess)
    except TypeError:
        print("Please enter a number.")
        continue
    
    chances += 1
    
    if guessNum == num:
        print("Your guess is correct! You took %d chances." % chances)
        break
    
    if guessNum > num:
        print("Your guess too HIGH.")
    else:
        print("Your guess is too LOW.")
        
    