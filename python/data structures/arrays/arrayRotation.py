"""
Program for array rotation
Write a function rotate(ar[], d, n) that rotates arr[] of size n by d elements.
[1,2,3,4,5,6,7] when rotated by d=2 would become [3,4,5,6,7,1,2]
"""
import array

def rotate(ar, d, n):
    for i in range(0, d):
        item = ar.pop(0)
        ar.append(item)

def main():
    arr = array.array('i', [1,2,3,4,5,6,7])
    
    #print arr
    print('Original Array : ', end = ' ')
    for i in range(0, len(arr)):
        print(arr[i], end = ' ')
    print('\n')

    rotate(arr, 3, len(arr))

    #print arr
    print('Rotated Array : ', end = ' ')
    for i in range(0, len(arr)):
        print(arr[i], end = ' ')
    print('\n') 

if __name__ == '__main__':
    main()