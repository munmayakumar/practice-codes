# Python code to demonstrate few array functions
  
# importing "array" for array operations

import array

# creating an array
arr = array.array('i', [11, 2, -33, 5])

print('The new created array is : ', end = ' ')
for i in range(0, len(arr)):
    print(arr[i], end = ' ')

print('\n')

#append to array
arr.append(10)

print('The appended array is : ', end = ' ')
for i in range(0, len(arr)):
    print(arr[i], end = ' ')

print('\n')

#insert at specific position in array
arr.insert(2, 100)

print('The array after insert is : ', end = ' ')
for i in range(0, len(arr)):
    print(arr[i], end = ' ')

print('\n')


#pop():- This function removes the element at the position mentioned in its argument and returns it.
x = arr.pop(3)
print('Element popped from position %d is %d' %(3,x))

print('The array after pop is : ', end = ' ')
for i in range(0, len(arr)):
    print(arr[i], end = ' ')

print('\n')

#remove():- This function is used to remove the first occurrence of the value mentioned in its arguments.
arr.remove(5)

print('The array after remove is : ', end = ' ')
for i in range(0, len(arr)):
    print(arr[i], end = ' ')

print('\n')

#index() :- This function returns the index of the first occurrence of value mentioned in arguments.
#reverse() :- This function reverses the array.
 