class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
        self.prev = None

class DoubleLinkedList:
    def __init__(self):
        self.head = None

    def printList(self):
        temp = self.head
        while (temp):
            data = temp.data
            next_data = 99
            prev_data = 99

            if (temp.next):
                next_data = temp.next.data
            if (temp.prev):
                prev_data = temp.prev.data
            print('%d>>%d>>%d' %(prev_data, data, next_data))
            temp = temp.next



if __name__ == "__main__":
    dlist = DoubleLinkedList()

    dlist.head = Node(1)
    second = Node(2)
    dlist.head.next = second
    second.prev = dlist.head

    third = Node(3)
    second.next = third
    third.prev = second

    fourth = Node(4)
    third.next = fourth
    fourth.prev = third

    fifth = Node(5)
    fourth.next = fifth
    fifth.prev = fourth

    dlist.printList()
