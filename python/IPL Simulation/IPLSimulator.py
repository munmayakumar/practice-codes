# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 01:04:45 2020

@author: admin
"""

from os import system, name
import random
import sys

leaderBoardPlayer = []
leaderBoardTeams = []

def clear(): 
    # for windows 
    if name == 'nt': 
        _ = system('cls') 


def createdLeaderBoardPlayer(players):
    for i in players:
        playerInfo = {'name':i['name'], 'wins':0, 'losses':0, 'score':0}
        leaderBoardPlayer.append(playerInfo)
       
    
def updateLeaderBoardPlayer(playerName, result, resultAmount):
    for i in range(0, len(leaderBoardPlayer)):
        if leaderBoardPlayer[i]['name'] == playerName:
            leaderBoardPlayer[i]['score'] += resultAmount
            if result == 'W':
                leaderBoardPlayer[i]['wins'] += 1
            if result == 'L':
                leaderBoardPlayer[i]['losses'] += 1

def printLeaderBoardPlayer(wid):
    print('Player'.ljust(wid) + 
          'W'.rjust(wid) +
          'L'.rjust(wid) +
          'Score'.rjust(wid))      
    
    print('-'*wid*4)
    
    for i in leaderBoardPlayer:
        print(i['name'].ljust(wid) 
              + str(i['wins']).rjust(wid)
              + str(i['losses']).rjust(wid)
              + str(i['score']).rjust(wid))
    

def createdLeaderBoardTeams(teams):
    for i in teams:
        teamInfo = {'team':i, 'played':0, 'wins':0, 'losses':0}
        leaderBoardTeams.append(teamInfo)
        

def updateLeaderBoardTeam(team1, team2, winTeam):
    for i in range(0, len(leaderBoardTeams)):
        if leaderBoardTeams[i]['team'] == team1:
            leaderBoardTeams[i]['played'] += 1
            if team1 == winTeam:
                leaderBoardTeams[i]['wins'] += 1
            else:
                leaderBoardTeams[i]['losses'] += 1
        if leaderBoardTeams[i]['team'] == team2:
            leaderBoardTeams[i]['played'] += 1
            if team2 == winTeam:
                leaderBoardTeams[i]['wins'] += 1
            else:
                leaderBoardTeams[i]['losses'] += 1


def printLeaderBoardTeam(wid):
    print('Team'.ljust(wid) + 
          'P'.rjust(wid) +
          'W'.rjust(wid) +
          'L'.rjust(wid)) 
    
    print('-'*wid*4)
    
    for i in leaderBoardTeams:
        print(i['team'].ljust(wid) 
              + str(i['played']).rjust(wid)
              + str(i['wins']).rjust(wid)
              + str(i['losses']).rjust(wid))
        
        

def simulateCurrentMatch(players, team1, team2, winTeam):
    currMatch = []
    totalBetAmount = 0
    winners = 0
    winningBetAmount = 0
    looers = 0
    loosingBetAmount = 0
    
    for i in players:
        #playnerName = i['name']
        
        #Find team to vote
        preference = i['preferredTeams']
        
        #if none of the teams are in preference list, vote random team
        if team1 not in preference and team2 not in preference:
            randInt = random.randint(0,1000)%2
            if randInt == 0:
                votedTeam = team1
            else:
                votedTeam = team2
        
        #if both the teams are in preference list, vote on priority order
        if team1 in preference and team2 in preference:
            if preference.index(team1) < preference.index(team2):
                votedTeam = team1
            else:
                votedTeam = team2
         
        #if only 1st team is in preference, vote team1
        if team1 in preference and team2 not in preference:
            votedTeam = team1
                
        #if only 1st team is in preference, vote team1
        if team1 not in preference and team2 in preference:
            votedTeam = team2
        
        #calcualte Bet amount
        betAmount = random.randint(i['betAmountRange'][0], i['betAmountRange'][1])
        totalBetAmount += betAmount
        
        #calculate total winning amount
        if votedTeam == winTeam:
            winners += 1
            winningBetAmount += betAmount
          
         #calculate total loosing amount
        if votedTeam != winTeam:
            looers += 1
            loosingBetAmount += betAmount
                    
        currMatchPlayer = {'name':i['name'], 'votedTeam':votedTeam, 'betAmount':betAmount, 'resultAmount':0}
        
        currMatch.append(currMatchPlayer)
        #print(playnerName + " " + str(i['betAmountRange'][0]) + " " + str(i['betAmountRange'][1]) + " " + str(betAmount) + " " + votedTeam)
                
    #calculate win/loss amount
    if winners > 0:
        for i in currMatch:
            if i['votedTeam'] == winTeam:
                i['resultAmount'] = round(loosingBetAmount*i['betAmount']/winningBetAmount)
                updateLeaderBoardPlayer(i['name'], 'W', i['resultAmount'])
            else:
                i['resultAmount'] = -1*i['betAmount']
                updateLeaderBoardPlayer(i['name'], 'L', i['resultAmount'])
    
    #print(currMatch)


def play(players, teams, totalMatches):
    totalTeams = len(teams)
    
    #create leaderboard for players
    createdLeaderBoardPlayer(players)
    
    #create leaderboard for teams
    createdLeaderBoardTeams(teams)

    #loop for number of matches
    for i in range(0, totalMatches):
        #decide teams to play
        while True:
            team1 = teams[random.randint(0,totalTeams-1)]
            team2 = teams[random.randint(0,totalTeams-1)]
            if team1 != team2:
                break
            
        #decide winner
        randInt = random.randint(0,1000)%2
        if randInt == 0:
            winTeam = team1
        else:
            winTeam = team2
        #print(str(i) + " : " + team1 + " vs " + team2 + " : Winner - " + winTeam)
        
        #update Team Leader Board
        updateLeaderBoardTeam(team1, team2, winTeam)
        
        #create Current Match - Votes, Bets, Amount won/lost
        simulateCurrentMatch(players, team1, team2, winTeam)
        
        clear()
        print('IPL SIMULATOR'.center(48, '*'))
        print('Running simulation ' + str(i+1) + ' of ' + str(totalMatches))
        print('')
        printLeaderBoardPlayer(12)
        print('')
        printLeaderBoardTeam(12)
        print('')
        

def main(loops):
    #create bots : add/modify bots here
    Bot1 = {'name':'BotArijit', 'betAmountRange':[200,200], 'preferredTeams':['KKR','MI'], 'description':'Bets 200 amount to preferred teams'}
    Bot2 = {'name':'BotPriyab', 'betAmountRange':[1000,1000], 'preferredTeams':['RCB'], 'description':'Bets 1000 amount to preferred teams'}
    Bot3 = {'name':'BotPramod', 'betAmountRange':[200,500], 'preferredTeams':['RCB', 'CSK'], 'description':'Bets 200-500 amount to preferred teams'}
    Bot4 = {'name':'BotSanthosh', 'betAmountRange':[500,1000], 'preferredTeams':['SRH','CSK'], 'description':'Bets 500-1000 amount to preferred teams'}
    Bot5 = {'name':'BotManob', 'betAmountRange':[400,800], 'preferredTeams':['KKR','RR', 'DC'], 'description':'Bets 400-800 amount to preferred teams'}
    Bot6 = {'name':'BotAnant', 'betAmountRange':[200,200], 'preferredTeams':[], 'description':'Bets 200 amount to random teams'}
    Bot7 = {'name':'BotShivam', 'betAmountRange':[1000,1000], 'preferredTeams':[], 'description':'Bets 1000 amount to random teams'}
    Bot8 = {'name':'BotMunmaya', 'betAmountRange':[200,500], 'preferredTeams':[], 'description':'Bets 200-500 amount to random teams'}
    Bot9 = {'name':'BotSharaf', 'betAmountRange':[500,1000], 'preferredTeams':[], 'description':'Bets 500-1000 amount to random teams'}
    Bot10 = {'name':'BotRima', 'betAmountRange':[200,500], 'preferredTeams':['RCB', 'MI'], 'description':'Bets 200-500 amount to random teams'}
    
    #list of bot players:  : add/modify bots here
    players = [Bot1, Bot2, Bot3, Bot4, Bot5, Bot6, Bot7, Bot8, Bot9, Bot10]
    
    #list of teams :  add/modify teams here
    teams = ['MI', 'DC', 'RCB', 'KKR', 'KXIP', 'RR', 'SRH', 'CSK', 'KUS']
        
    #number of matches
    totalMatches = int(loops)
    
    #play!!
    play(players, teams, totalMatches)
    
    #clear()
    #printLeaderBoardPlayer(12)
    #print('')
    #print('')
    #printLeaderBoardTeam(12)
    
    #print Bot info
    for i in players:
        print(i['name'] + ' - ' + i['description'], end='')
        if len(i['preferredTeams']) > 0:
            print(' : ', end='')
            for j in i['preferredTeams']:
                print(j+' ', end='')
        print()
    print('')

if __name__ == '__main__':
    main(sys.argv[1])